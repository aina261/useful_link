$(function () {

    function log($var) {
        console.log($var);
    }
    function documentLog($var) {
        $(document).keyup(function () {
            console.log($var);
        });
    }

    $('.error').hide();

    let $menu = $('#menutoggle');
    let $menuButtonHover = $('#menuButton');
    let $burgerMenu = $('#svgBurger');
    let $crossMenu = $('#svgCross');
    let $menuOverlay = $('#menuOverlay');
    let $userIcon = $('#userIcon');
    let $userIconSvg = $('.userIconSvg');
    let $userMenu = $('#userMenu');
    let $arrow = $('.arrow');

    // form register
    let $formRegister = $('#formRegister');
    let $userName = $('#user_name_register');
    let $mailForm = $('#mail_register');
    let $pswForm = $('#password_register');
    let $userNameInfo = $('#userNameInfo');
    let $mailInfo = $('#mailInfo2');

    let mail = false;
    let user = false;

    // Profil Form Edit
    let $mailProfil = $('#mailProfil');

    // Form login
    let $userNameLogin = $('#user_name_login');
    let $passwordLogin = $('#password_login');

    // Uploaded link
    let $uploadedForm = $('#uploadedForm');
    let $buttonAddLink = $('#buttonAddLink');

    // ADD & MODIF LINK
    let $descriptionInput = $('#description');
    let $descriptionInputCount = $('#descCount');




    // Burger menu hover
    $menuButtonHover.mouseenter(function () {
       $('.svgHover').css('fill', '#3a3a3a');
    });

    $menuButtonHover.mouseleave(function () {
        $('.svgHover').css('fill', '#fff');
    });

    // USER MENU HOVER
    $userIcon.mouseenter(function () {
        $userIconSvg.css('fill', 'gold');
    });

    $userIcon.mouseleave(function () {
        $userIconSvg.css('fill', '#ededed');
    });

    // Close successful alert
    setTimeout(function () {
        $('.alert-success').fadeOut('fast');
    }, 5000);

    // On click, open or close main menu
    let $menuOpen = false;
    $menuButtonHover.click(function () {
        if (!$menuOpen) {
            $burgerMenu.fadeOut('fast');
            setTimeout(function () {
                $menu.addClass('menuOpen');
                $crossMenu.fadeIn('fast');
            }, 200);
            $menuOpen = true;
        } else {
            $crossMenu.fadeOut('fast');
            $menu.removeClass('menuOpen');
            setTimeout(function () {
                $burgerMenu.fadeIn('fast');
            }, 200);
            $menuOpen = false;
        }
    });


    // Open ul menu
    let typeMenuOpen = false;
    let languageMenuOpen = false;
    let libraryMenuOpen = false;
    let frameworkJsMenuOpen = false;
    let frameworkPhpMenuOpen = false;
    let cmsMenuOpen = false;

    $('#menuTitle_type').on('click', function() {
        if (!typeMenuOpen) {
            $('#menuItems_type').addClass('menuOpen');
            typeMenuOpen = true;
        } else {
            $('#menuItems_type').removeClass('menuOpen');
            typeMenuOpen = false;
        }
    });
    $('#menuTitle_language').on('click', function() {
        if (!languageMenuOpen) {
            $('#menuItems_language').addClass('menuOpen');
            languageMenuOpen = true;
        } else {
            $('#menuItems_language').removeClass('menuOpen');
            languageMenuOpen = false;
        }
    });
    $('#menuTitle_library').on('click', function() {
        if (!libraryMenuOpen) {
            $('#menuItems_library').addClass('menuOpen');
            libraryMenuOpen = true;
        } else {
            $('#menuItems_library').removeClass('menuOpen');
            libraryMenuOpen = false;
        }
    });
    $('#menuTitle_frameworkJs').on('click', function() {
        if (!frameworkJsMenuOpen) {
            $('#menuItems_frameworkJs').addClass('menuOpen');
            frameworkJsMenuOpen = true;
        } else {
            $('#menuItems_frameworkJs').removeClass('menuOpen');
            frameworkJsMenuOpen = false;
        }
    });
    $('#menuTitle_frameworkPhp').on('click', function() {
        if (!frameworkPhpMenuOpen) {
            $('#menuItems_frameworkPhp').addClass('menuOpen');
            frameworkPhpMenuOpen = true;
        } else {
            $('#menuItems_frameworkPhp').removeClass('menuOpen');
            frameworkPhpMenuOpen = false;
        }
    });
    $('#menuTitle_cms').on('click', function() {
        if (!cmsMenuOpen) {
            $('#menuItems_cms').addClass('menuOpen');
            cmsMenuOpen = true;
        } else {
            $('#menuItems_cms').removeClass('menuOpen');
            cmsMenuOpen = false;
        }
    });

    // On click, open or close user menu
    let $userMenuInfo = false;
    $userIcon.on('click', function () {
        if (!$userMenuInfo) {
            $userMenu.addClass('userMenuOpen');
            $arrow.addClass('arrowTurn');
            $userMenuInfo = true;
            // $('#mainContent').addClass('menuOpen');
        } else {
            $userMenu.removeClass('userMenuOpen');
            $arrow.removeClass('arrowTurn');
            $userMenuInfo = false;
            // $('#mainContent').removeClass('menuOpen');
        }
    });

    // On click main, close main and user menu
    $('main').on('click', function () {
        $crossMenu.fadeOut('fast');
        $menu.removeClass('menuOpen');
        setTimeout(function () {
            $menuOverlay.fadeOut('fast');
            $burgerMenu.fadeIn('fast');
        }, 200);
        $menuOpen = false;
        $userMenu.removeClass('userMenuOpen');
        $arrow.removeClass('arrowTurn');
        $userMenuInfo = false;
    });




    let $error = false;

    // Validating form register
    // Mail
    function validateMail($value) {
        $mailInfo.html('');
        $value.keyup(function () {
            // Regex mail
            function isValidEmailAddress(mail) {
                let pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return pattern.test(mail);
            }

            if ($value.val() === "") {
                $('#mailInfo').html('');
                mail = false;
            } else if (isValidEmailAddress($value.val())) {
                $('#mailInfo').html('Perfect, let\'s go for spam').css('color', "#BADA55");

                $.getJSON(
                    'inc/validateMail.php',
                    {
                        mail: $value.val(),
                    },
                    function (result) {
                       switch (result.success) {
                            case false:
                                $mailInfo.html('');
                                $value.css('borderColor', 'inherit');
                                mail = true;
                                break;

                            case true:
                                $('#mailInfo').html('');
                                $mailInfo.html('Sorry, this mail is already taken. Please, choose another one').css('color', '#E05959');
                                $value.css('borderColor', 'red');
                                mail = false;
                                break;

                            default:
                                $mailInfo.html('');
                        }
                    });

            } else {
                $('#mailInfo').html('Invalid E-mail address').css('color', '#E05959');
                mail = false;
            }
        });
    }


    // Password
    let characters = 0;
    let capitalletters = 0;
    let loweletters = 0;
    let number = 0;
    let special = 0;

    let upperCase= new RegExp('[A-Z]');
    let lowerCase= new RegExp('[a-z]');
    let numbers = new RegExp('[0-9]');
    let specialchars = new RegExp('[^\w]');




    // ====================================//
    // FOCUS ON PASSWORD INPUT
    // ====================================//
    $pswForm.keyup( function () {
        if ($pswForm.val().length < 5) {
            $('.infoPwd').fadeIn();
        }
        if ($pswForm.val().length >= 5) {
            $('.infoPwd').fadeOut();
        }
    });

    $pswForm.keyup( function () {

        if( $pswForm.val().length >= 1 ) {
            $pswForm.css('borderColor', 'initial');
        }
        if ($pswForm.val().match(upperCase)) { capitalletters = 1 } else { capitalletters = 0; }
        if ($pswForm.val().match(lowerCase)) { loweletters = 1 }  else { loweletters = 0; }
        if ($pswForm.val().match(numbers)) { number = 1 }  else { number = 0; }
        if ($pswForm.val().match(specialchars)) { special = 1 }  else { special = 0; }

        let total = characters + capitalletters + loweletters + number + special;

        get_total(total);
    });

    function get_total(total) {

        if (total === 0) {
            $('#passwordInfo').html('');
        } else if (total <= 2){
            $('#passwordInfo').html('Oooh look this tiny password. He\'s so cute !!').css('color', '#E05959');
        } else if(total === 3){
            $('#passwordInfo').html('Just a little more effort !').css('color', '#ff9000');
        } else {
            $('#passwordInfo').html('This a heavy password. I\'m proud of you !').css('color', "#BADA55");
        }
    }


    // Ajax for validate if nickname or mail is already use
    function validateUser() {

        $userNameInfo.html('');

        $.getJSON(
            'inc/validateUser.php',
            {
                user_name: $userName.val(),
            },
            function (result) {
                switch (result.success) {
                    case false:
                        $userNameInfo.html('Your user name is free').css('color', "#BADA55");
                        $userName.css('borderColor', 'initial');
                        user = true;
                        break;

                    case true:
                        $userNameInfo.html('Sorry, this user name is already taken ... ').css('color', '#E05959');
                        $userName.css('borderColor', 'red');
                        user = false;
                        break;

                    default:
                        $userNameInfo.html('');
                        user = false;
                }
            });
    }

    $userName.keyup(validateUser);
    $mailForm.keyup(validateMail($mailForm));



    // ==================== //
    // SUBMIT REGISTER
    // ==================== //
    $formRegister.on('submit', function (e) {
        $error = false;
        log(mail);

        if (!user || $userName.val() < 1) {
            $userName.css('borderColor', 'red');
            $error = true;
            e.preventDefault();
        }
        if (!mail || $mailForm.val() < 1) {
            $mailForm.css('borderColor', 'red');
            $error = true;
            log(mail);
            e.preventDefault();
        }
        if ($pswForm.val() < 5) {
            $pswForm.css('borderColor', 'red');
            $error = true;
            e.preventDefault();
        }
        // If error register / Log
        if ($error) {
            $('.error').show();
        } else {
            $('.error').hide();
        }
    });




    // LOGIN FORM
    $userNameLogin.keyup(function () {
        $userNameLogin.css('borderColor', 'initial');
    });

    $passwordLogin.keyup( function () {
        $userNameLogin.css('borderColor', 'initial');
    });



    $('#formLogin').on('submit', function (e) {
        $error = false;

        if ($userNameLogin.val() < 1 ) {
            $userNameLogin.css('borderColor', 'red');
            $error = true;
            e.preventDefault();
        }
        if ($passwordLogin.val() < 1 ) {
            $passwordLogin.css('borderColor', 'red');
            $error = true;
            e.preventDefault();
        }

        // If error register / Log
        if ($error) {
            $('.error').show();
        } else {
            $('.error').hide();
        }
    });




    // OPEN UPLOADED NEW LINK FROM
    let openAddLink = false;
    $buttonAddLink.on('click', function () {
        if (!openAddLink) {
            $uploadedForm.fadeIn('fast');
            openAddLink = true;
            $('#buttonAddLink').html('<i class="fas fa-times"></i>')
            $('.uploaded').addClass('mb-5');
            $('.infoButton').html('Close the form');
        } else {
            $uploadedForm.fadeOut('fast');
            openAddLink = false;
            $('#buttonAddLink').html('<i class="fas fa-plus"></i>')
            $('.uploaded').removeClass('mb-5');
            $('.infoButton').html('Add new link');
        }
    });

    $buttonAddLink.mouseenter( function () {
       $('.infoButton').css('width', '200px');
    });

    $buttonAddLink.mouseleave( function () {
        $('.infoButton').css('width', '0');
    });



    // ============================ //
    //      Description < 255 chars
    // ============================ //
    $descriptionInput.keyup(function () {
        $descriptionInputCount.html($descriptionInput.val().length + "/255");

        if ($descriptionInput.val().length  === 0) {
            $descriptionInputCount.html("");
        }
        if ($descriptionInput.val().length < 150) {
            $descriptionInputCount.css('color', 'lime');
        }
        if ($descriptionInput.val().length >= 150) {
            $descriptionInputCount.css('color', 'orange');
        }
        if ($descriptionInput.val().length >= 220) {
            $descriptionInputCount.css('color', 'red');
        }
    });
    
    
    
    // UPLOADED and EDIT FORM LINK VALIDATE
    $uploadedForm.on('submit', function (e) {
       if ($('#title').val().length < 1) {
           $('#title').css('borderColor', 'red');
           e.preventDefault();
       }
        if ($('#link').val().length < 1) {
            $('#link').css('borderColor', 'red');
            e.preventDefault();
        }
        if ($descriptionInput.val().length < 1) {
            $descriptionInput.css('borderColor', 'red');
            e.preventDefault();
        }
        if ($('#type').val() === '-- Make your choice --') {
            $('#type').css('borderColor', 'red');
            e.preventDefault();
        }
    });

    $('#title').keyup(function () {
        $('#title').css('borderColor', 'initial');
    });

    $('#link').keyup(function () {
        $('#link').css('borderColor', 'initial');
    });

    $descriptionInput.keyup(function () {
        $descriptionInput.css('borderColor', 'initial');
    });

    $('#type').change(function () {
        $('#type').css('borderColor', 'initial');
    });






    // EDITING PROFIL
    let editProfil = false;
    $('#editProfil').on('click', function () {
       if (!editProfil) {
           editProfil = true;
           $('.editProfilForm').fadeIn('fast');
           $('#editProfil').html('Close edit profil');
       } else {
           editProfil = false;
           $('.editProfilForm').fadeOut('fast');
           $('#editProfil').html('Edit profil');
       }
    });

    $mailProfil.keyup(validateMail($mailProfil));



}); // END LOAD
-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 02 nov. 2018 à 13:44
-- Version du serveur :  5.7.19
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `useful_link`
--

-- --------------------------------------------------------

--
-- Structure de la table `link`
--

CREATE TABLE `link` (
  `id_link` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` enum('language','library','framework','cms','tutorial','unit test','git','Utilities','Other') NOT NULL,
  `language` enum('html','css','javascript','typescript','php','sql','java','asp_dot_net') DEFAULT NULL,
  `library` enum('jquery','bootstrap') DEFAULT NULL,
  `framework_js` enum('angularjs','angular','vuejs','reactjs','react_native','emberjs','meteorjs') DEFAULT NULL,
  `framwork_php` enum('symfony','laravel','sakephp','codeigniter','zend','yii') DEFAULT NULL,
  `cms` enum('wordpress','drupal','joomla','botclear','weebly','squarespace','emonsite','ipaoo','sitew','cmonsite','jimbo','shopify','kiubi','orson') DEFAULT NULL,
  `love` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(255) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('developper','former','student','ceo','cto','other') NOT NULL,
  `access` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `mail`, `password`, `status`, `access`) VALUES
(1, 'Aina', 'a.ramiarasoa@gmail.com', '', 'developper', '1'),
(9, 'Arnaud', 'mplurv@gmail.com', '$2y$10$4fYjUE69OzcaAgZc7bg3Yeza3hEWQYbkcdzAmJIINKkVsUSb/s9QW', 'developper', '0'),
(10, 'Arnaud', 'mplurv@gmail.com', '$2y$10$NJ93zdsZMZnvT0Dcqa0s6uh/sr.mahePzsSnMBbcNemzncv6iyxu6', 'developper', '0');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id_link`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `link`
--
ALTER TABLE `link`
  MODIFY `id_link` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `link`
--
ALTER TABLE `link`
  ADD CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

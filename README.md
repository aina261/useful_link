# USEFUL LINK
```
{
  "name": "useful_link",
  "version": "0.0.0",
  "description": "Useful link sharing for web developper",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://aina261@bitbucket.org/aina261/useful_link.git"
  },
  "author": "Arnaud RAMIARASOA",
  "license": "MIT",
  "homepage": "https://bitbucket.org/aina261/useful_link#readme"
}
    
```

## LANGUAGES USED

```
{
    "languages": "html5, css3, javascript ( jQuery, Ajax ), php 7.2"
}
```

## BDD DESIGN

```
> USER
    > id_user       PK
    > nickname
    > mail
    > password
    > status            ENUM "developper; former; student; ceo; cto; other"
    > access            ENUM "0, 1"
    
> LINK
    > id_link       PK
    > id_user       FK
    > link
    > description
    > type              ENUM "language; library; framework; cms; unit test; git; compagny; linkedIn; tutorial"
    > language      N   ENUM "html; css; javascript; typescript; php; java; asp.net; sql;
    > library       N   ENUM "jquery; bootsrap;
    > frameworkjs    N   ENUM "angularjs; angular; vuejs; reactjs; reactnative; emberjs; meteorjs"
    > frameworkphp   N   ENUM "laravel; symfony; cakephp; codeigniter; zend; yii"
    > cms           N   ENUM "drupal; wordpress; joomla; botclear; weebly; squarespace; e-monsite; ipaoo; orson; sitew; cmonsite; jimdo; shopify: kiubi
    
```


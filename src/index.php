<?php

$title = "Latest useful link";
require_once ('inc/init.php');

$result = $pdo->query("SELECT * FROM link ORDER BY id_link DESC");
$result->execute();
$links = $result->fetchAll();


foreach ($links as $link) {

    $id_link = $link['id_link'];

    $user = $pdo->query("SELECT user_name 
                                  FROM user 
                                  WHERE id_user = (
                                      SELECT id_user
                                      FROM link 
                                      WHERE id_link = $id_link)");
    $user->execute();
    $user = $user->fetch();

    $linkG = "";
    if ($link['link']) {
        if (linkG($link['link'])) {
            $linkG = $link['link'];
        } else {
            $linkG = "http://" . $link['link'];
        }
    }

    // debug($link, 1);

    $linkCard .= "<div class='card mb-3'>";
    if ($link['og_img'] != NULL ) {
        $linkCard .= "<img class='card-img-top' src='$link[og_img]' alt='Card image cap'>";
    } else {
        $linkCard .= "<img class='card-img-top' src='" . URL . "asset/img/img_not_available.jpg' alt='Card image cap'>";
    }
    $linkCard .= "<div class='card-body'>";
    $linkCard .= "<p class='sending'>Sending by $user[user_name] @ $link[date_uploaded] - $link[time_uploaded]</p>";
    $linkCard .= "<h5 class='card-title'>$link[title]</h5>";
    $linkCard .= "<p class='card-text'>$link[description]</p>";
    $linkCard .= "<a href='$linkG' target='_blank' class='btn btn-secondary'>Follow the link</a>";
    $linkCard .= "</div>";
    $linkCard .= "<div class='card-footer'>";
    if (isset($link['type'])) {
        $linkCard .= "<a href='localized.php?type=$link[type]'>$link[type]</a>";
    }
    if (isset($link['language'])) {
        $linkCard .= "<a href='localized.php?language=$link[language]'>$link[language]</a>";
    }
    if (isset($link['library'])) {
        $linkCard .= "<a href='localized.php?library=$link[library]'>$link[library]</a>";
    }
    if (isset($link['framework_js'])) {
        $linkCard .= "<a href='localized.php?frameworkjs=$link[framework_js]'>$link[framework_js]</a>";
    }
    if (isset($link['framwork_php'])) {
        $linkCard .= "<a href='localized.php?frameworkphp=$link[framwork_php]'>$link[framwork_php]</a>";
    }
    if (isset($link['cms'])) {
        $linkCard .= "<a href='localized.php?cms=$link[cms]'>$link[cms]</a>";
    }
    $linkCard .= "</div>";
    $linkCard .= "</div>";
}

if ($_GET) {
    if (isset($_GET['up']) && $_GET['up'] == 'success') {
        $msg = "<div class='alert alert-success' role='alert'>Your link was successful uploaded</div>";
    }
}




require_once ('inc/addLinkForm.php');
require_once ('inc/header.php');


?>

<div class="row justify-content-center">
    <?= $msg ?>
    <h1 class="text-center"><?= $title ?></h1>
</div>
<div class="row justify-content-center">
    <?= $linkCard ?>
</div>





<?php require_once ('inc/footer.php'); ?>
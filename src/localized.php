<?php

$title = "Error 404";
$content = "Sorry, nothing to see here ...";

if ($_GET) {
    if (isset($_GET['type'])) {
        if ($_GET['type'] == 'unittest'){
            $title = "Unit Test";
        } else {
            $title = $_GET['type'];
        }
    }
    if (isset($_GET['language'])) {
        if ($_GET['language'] == "js") {
            $title = "Java script";
        } elseif ($_GET['language'] == "typescript") {
            $title = "Type Script";
        } else {
            $title = $_GET['language'];
        }
    }
    if (isset($_GET['library'])) {
        $title = $_GET['library'];
    }
    if (isset($_GET['frameworkjs'])) {
        if ($_GET['frameworkjs'] == 'reactnative') {
            $title = "React Native";
        } elseif ($_GET['frameworkjs'] == 'angularjs') {
            $title = "Angular JS";
        } elseif ($_GET['frameworkjs'] == 'vuejs') {
            $title = "Vue js";
        } elseif ($_GET['frameworkjs'] == 'reactjs') {
            $title = "React js";
        } elseif ($_GET['frameworkjs'] == 'emberjs') {
            $title = "ember js";
        } elseif ($_GET['frameworkjs'] == 'meteorjs') {
            $title = "Meteor js";
        } else {
            $title = $_GET['frameworkjs'];
        }
    }
    if (isset($_GET['frameworkphp'])) {
        $title = $_GET['frameworkphp'];
    }
    if (isset($_GET['cms'])) {
        if ($_GET['cms'] == "emonsite") {
            $title = "E-monsite";
        } else {
            $title = $_GET['cms'];
        }
    }
}


require_once ('inc/init.php');


if ($_GET) {
    if (isset($_GET['type'])) {
        $content = localizedLink("type", $_GET['type']);
    }
    if (isset($_GET['language'])) {
        $content = localizedLink("language", $_GET['language']);
    }
    if (isset($_GET['library'])) {
        $content = localizedLink("library", $_GET['library']);
    }
    if (isset($_GET['frameworkjs'])) {
        $content = localizedLink("framework_js", $_GET['frameworkjs']);
    }
    if (isset($_GET['frameworkphp'])) {
        $content = localizedLink("framework_php", $_GET['frameworkphp']);
    }
    if (isset($_GET['cms'])) {
        $content = localizedLink("cms", $_GET['cms']);
    }
}



require_once ('inc/addLinkForm.php');
require_once ('inc/header.php');


?>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1 class="text-center"><?= $title ?></h1>
            <?= $msg ?>
        </div>
    </div>
    <div class="row justify-content-center">
        <?= $content ?>
    </div>





<?php require_once ('inc/footer.php'); ?>
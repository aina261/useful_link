
<?php

if ($_GET) {
    if (isset($_GET['disconnect']) && $_GET['disconnect'] == "true") {
        session_destroy();
        header('location:index.php');
    }
}
require_once ('addLinkForm.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= URL ?>asset/img/favicon.png">
    <meta name="author" content="Arnaud RAMIARASOA"/>
    <meta name="Copyright" content="Arnaud RAMIARASOA"/>
    <meta name="theme-color" content="#3A3A3A"/>
    <meta name="description" content="Share my link is a website dedicated to sharing link about web development."/>

    <meta property="og:locale" content="en_EN"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Share-my-link.com"/>
    <meta property="og:description" content="Share my link is a website dedicated to sharing link about web development."/>
    <meta property="og:image" content="<?= URL ?>asset/img/logo.jpg"/>
    <meta property="og:url" content="https://share-my-link.com"/>
    <meta property="og:site_name" content="share-my-link.com"/>

    <meta name="robots" content="noindex">

    <title>Share my Link | <?= $title ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122819611-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-122819611-2');
    </script>

</head>
<body>

<div class="headerContent">
    <header>
        <a id="navLogo" class="navbar-brand" href="index.php">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill-rule="evenodd" clip-rule="evenodd"><path d="M14.851 11.923c-.179-.641-.521-1.246-1.025-1.749-1.562-1.562-4.095-1.563-5.657 0l-4.998 4.998c-1.562 1.563-1.563 4.095 0 5.657 1.562 1.563 4.096 1.561 5.656 0l3.842-3.841.333.009c.404 0 .802-.04 1.189-.117l-4.657 4.656c-.975.976-2.255 1.464-3.535 1.464-1.28 0-2.56-.488-3.535-1.464-1.952-1.951-1.952-5.12 0-7.071l4.998-4.998c.975-.976 2.256-1.464 3.536-1.464 1.279 0 2.56.488 3.535 1.464.493.493.861 1.063 1.105 1.672l-.787.784zm-5.703.147c.178.643.521 1.25 1.026 1.756 1.562 1.563 4.096 1.561 5.656 0l4.999-4.998c1.563-1.562 1.563-4.095 0-5.657-1.562-1.562-4.095-1.563-5.657 0l-3.841 3.841-.333-.009c-.404 0-.802.04-1.189.117l4.656-4.656c.975-.976 2.256-1.464 3.536-1.464 1.279 0 2.56.488 3.535 1.464 1.951 1.951 1.951 5.119 0 7.071l-4.999 4.998c-.975.976-2.255 1.464-3.535 1.464-1.28 0-2.56-.488-3.535-1.464-.494-.495-.863-1.067-1.107-1.678l.788-.785z"/></svg>
            <div  class="title">
                <span class="masterTitle">
                Share my Link
                </span>
                <span class="info">Made with <img src="asset/img/heart.png" alt="heart image" title="With all my love"> for web developper</span>
            </div>
        </a>

        <div id="menuIcon">
            <?php
                if ($_SESSION) {
                    if (isset($_SESSION['user'])) {
                        $nameUser = $_SESSION['user']['user_name'];
                        echo "<div id='hello'>";
                        echo "<p>Hello $nameUser</p>";
                        echo "</div>";
                        if ($_SESSION['user']['access'] === '1' || $_SESSION['user']['access'] === '2') {
                            echo "<div id='admin'>";
                            echo "<a href='admin/'>Admin</a>";
                            echo "</div>";
                        }
                    }
                }
            ?>

            <div id="userIcon">
                <svg class="userIconSvg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 22c-3.123 0-5.914-1.441-7.749-3.69.259-.588.783-.995 1.867-1.246 2.244-.518 4.459-.981 3.393-2.945-3.155-5.82-.899-9.119 2.489-9.119 3.322 0 5.634 3.177 2.489 9.119-1.035 1.952 1.1 2.416 3.393 2.945 1.082.25 1.61.655 1.871 1.241-1.836 2.253-4.628 3.695-7.753 3.695z"/></svg>
                <svg class="userIconSvg arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.028 0v6.425l5.549 5.575-5.549 5.575v6.425l11.944-12z"/></svg>
            </div>

            <button class="btn btn-sm btn-outline-warning menuButton" id="menuButton" type="button">
                <svg id="svgBurger" class="svgHover" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill-rule="evenodd" clip-rule="evenodd">
                    <path d="M24 18v1h-24v-1h24zm0-6v1h-24v-1h24zm0-6v1h-24v-1h24z"></path>
                    <path d="M24 19h-24v-1h24v1zm0-6h-24v-1h24v1zm0-6h-24v-1h24v1z"></path>
                </svg>

                <svg id="svgCross" class="svgHover" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill-rule="evenodd" clip-rule="evenodd">
                    <path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293
                10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"></path>
                </svg>
            </button>

        </div>
    </header>


    <nav id="userMenu">
        <ul>
            <?php if (!isset($_SESSION['user'])) { echo "<li><a href='register.php'>Register</a></li>"; } ?>
            <?php if (!isset($_SESSION['user'])) { echo "<li><a href='login.php'>Login</a></li>"; } ?>
            <?php if (isset($_SESSION['user'])) { echo "<li><a href='profil.php'>Profil</a></li>"; } ?>
            <?php if (isset($_SESSION['user'])) { echo "<li><a href='?disconnect=true'>Disconnect</a></li>"; } ?>
        </ul>
    </nav>

    <nav id="menutoggle" class="menu">
        <div class="content">
            <div class="type contentBlock">
                <h3 id="menuTitle_type">TYPE</h3>
                <ul id="menuItems_type" class="menuItems">
                    <li><a href="localized.php?type=language">Language</a></li>
                    <li><a href="localized.php?type=library">Library</a></li>
                    <li><a href="localized.php?type=framework">Framework</a></li>
                    <li><a href="localized.php?type=cms">CMS</a></li>
                    <li><a href="localized.php?type=unittest">Unit Test</a></li>
                    <li><a href="localized.php?type=tutorial">Tutoria</a>l</li>
                    <li><a href="localized.php?type=git">Git</a></li>
                    <li><a href="localized.php?type=utilities">Utilities</a></li>
                    <li><a href="localized.php?type=front">Front</a></li>
                    <li><a href="localized.php?type=back">Back</a></li>
                    <li><a href="localized.php?type=other">Other</a></li>
                </ul>
            </div>
            <div class="language contentBlock">
                <h3 id="menuTitle_language">Language</h3>
                <ul id="menuItems_language" class="menuItems">
                    <li><a href="localized.php?language=html">HTML</a></li>
                    <li><a href="localized.php?language=css">CSS</a></li>
                    <li><a href="localized.php?language=javascript">Java Script</a></li>
                    <li><a href="localized.php?language=typescript">TypeScript</a></li>
                    <li><a href="localized.php?language=php">PHP</a></li>
                    <li><a href="localized.php?language=java">Java</a></li>
                    <li><a href="localized.php?language=aspnet">ASP.net</a></li>
                    <li><a href="localized.php?language=sql">SQL</a></li>
                </ul>
            </div>
            <div class="library contentBlock">
                <h3 id="menuTitle_library">Library</h3>
                <ul id="menuItems_library" class="menuItems">
                    <li><a href="localized.php?library=jquery">jQuery</a></li>
                    <li><a href="localized.php?library=bootstrap">Bootstrap</a></li>
                </ul>
            </div>
            <div class="frameworkJs contentBlock">
                <h3 id="menuTitle_frameworkJs">Framework JS</h3>
                <ul id="menuItems_frameworkJs" class="menuItems">
                    <li><a href="localized.php?frameworkjs=angularjs">AngularJS</a></li>
                    <li><a href="localized.php?frameworkjs=angular">Angular</a></li>
                    <li><a href="localized.php?frameworkjs=vuejs">VueJS</a></li>
                    <li><a href="localized.php?frameworkjs=reactjs">ReactJS</a></li>
                    <li><a href="localized.php?frameworkjs=reactnative">React native</a></li>
                    <li><a href="localized.php?frameworkjs=emberjs">EmberJS</a></li>
                    <li><a href="localized.php?frameworkjs=meteorjs">MeteoJS</a></li>
                </ul>
            </div>
            <div class="framworkPhp contentBlock">
                <h3 id="menuTitle_frameworkPhp">Framework PHP</h3>
                <ul id="menuItems_frameworkPhp" class="menuItems">
                    <li><a href="localized.php?frameworkphp=symfony">Symfony</a></li>
                    <li><a href="localized.php?frameworkphp=laravel">Laravel</a></li>
                    <li><a href="localized.php?frameworkphp=cakephp">CakePHP</a></li>
                    <li><a href="localized.php?frameworkphp=codeigniter">CodeIgniter</a></li>
                    <li><a href="localized.php?frameworkphp=zen">Zen</a></li>
                    <li><a href="localized.php?frameworkphp=yii">YII</a></li>
                </ul>
            </div>
            <div class="cms contentBlock">
                <h3 id="menuTitle_cms">CMS</h3>
                <ul id="menuItems_cms" class="menuItems">
                    <li><a href="localized.php?cms=wordpress">Wordpress</a></li>
                    <li><a href="localized.php?cms=magento">Magento</a></li>
                    <li><a href="localized.php?cms=prestashop">Prestashop</a></li>
                    <li><a href="localized.php?cms=drupal">Drupal</a></li>
                    <li><a href="localized.php?cms=joomla">Joomla</a></li>
                    <li><a href="localized.php?cms=botclear">Botclear</a></li>
                    <li><a href="localized.php?cms=weebly">Weebly</a></li>
                    <li><a href="localized.php?cms=squarespace">SquareSpace</a></li>
                    <li><a href="localized.php?cms=emonsite">E-monsite</a></li>
                    <li><a href="localized.php?cms=ipaoo">Ipaoo</a></li>
                    <li><a href="localized.php?cms=orson">Orson</a></li>
                    <li><a href="localized.php?cms=sitew">Sitew</a></li>
                    <li><a href="localized.php?cms=cmonsite">Cmonsite</a></li>
                    <li><a href="localized.php?cms=jimdo">Jimdo</a></li>
                    <li><a href="localized.php?cms=shopify">Shopify</a></li>
                    <li><a href="localized.php?cms=kiubi">Kiubi</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<main id="mainContent">
    <div class="container-fluid">

    <?php
        if (isset($_SESSION['user'])) {
            echo $newLinkForm;
        }
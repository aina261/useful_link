<?php
require_once ('../inc/init.php');

if ($_GET) {
    if (isset($_GET['aUser']) && $_GET['aUser'] == "delete" && is_numeric($_GET['id'])) {
        $result = $pdo->prepare('SELECT * FROM user WHERE id_user = :id_user');
        $result->bindValue(':id_user', $_GET['id'], PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() == 1) {
            $user = $result->fetch();
            $delete_req = "DELETE FROM user WHERE id_user = $user[id_user]";
            $delete_result = $pdo->exec($delete_req);
            if ($delete_result) {
                header('Location:?delete=success');
            } else {
                header('Location:?delete=failed');
            }

        }
    }

    if (isset($_GET['delete'])) {
        if ($_GET['delete'] == "success") {
            $msg .= "<div class='alert alert-success ml-5' role='alert'>The user has been successful deleted</div>";
        } else {
            $msg .= "<div class='alert alert-danger ml-5' role='alert'>An error has occured</div>";
        }
    }
}

$result = $pdo->query("SELECT * FROM user ORDER BY id_user DESC");
$result->execute();
$users = $result->fetchAll();


$linkCard .= "<table id='listLink'>";
$linkCard .= "<th>";
$linkCard .= "<tr>";

$linkCard .= "<td>";
$linkCard .= "<h5>Id_ User</h5>";
$linkCard .= "</td>";

$linkCard .= "<td>";
$linkCard .= "<h5>User_Name</h5>";
$linkCard .= "</td>";

$linkCard .= "<td>";
$linkCard .= "<h5>Mail</h5>";
$linkCard .= "</td>";

$linkCard .= "<td>";
$linkCard .= "<h5>Status</h5>";
$linkCard .= "</td>";

$linkCard .= "<td>";
$linkCard .= "<h5>Access</h5>";
$linkCard .= "</td>";

$linkCard .= "<td>";
$linkCard .= "<h5>Delete</h5>";
$linkCard .= "</td>";

$linkCard .= "</tr>";
$linkCard .= "</th>";
$linkCard .= "<tb>";

foreach ($users as $user) {
    $linkCard .= "<tr class='borderLink'>";

    $linkCard .= "<td>";
    $linkCard .= "<span>$user[id_user]</span>";
    $linkCard .= "</td>";

    $linkCard .= "<td>";
    $linkCard .= "<span>$user[user_name]</span>";
    $linkCard .= "</td>";

    $linkCard .= "<td>";
    $linkCard .= "<span>$user[mail]</span>";
    $linkCard .= "</td>";

    $linkCard .= "<td>";
    $linkCard .= "<span>$user[status]</span>";
    $linkCard .= "</td>";

    $linkCard .= "<td>";
    $linkCard .= "<span>$user[access]</span>";
    $linkCard .= "</td>";

    $linkCard .= "<td class='listLinkDelete'>";
    $linkCard .= "<a data-toggle='modal' data-target='#deleteModal$user[id_user]'><i class='fas fa-ban'></i></a>";
    $linkCard .= "</td>";
    $linkCard .= deleteModal($user['id_user'], $user['user_name'], "deleteUserAdmin");
    $linkCard .= "</tr>";
}

$linkCard .= "</tb>";
$linkCard .= "</table>";




require_once ('inc/header.php');

?>

    <div class="col-12">
        <?= $msg ?>
        <?= $linkCard ?>
    </div>

<?php
require_once ('inc/footer.php');
?>
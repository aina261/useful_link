-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 02 nov. 2018 à 21:29
-- Version du serveur :  5.7.19
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `useful_link`
--

-- --------------------------------------------------------

--
-- Structure de la table `link`
--

CREATE TABLE `link` (
  `id_link` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `title` varchar(150) NOT NULL,
  `link` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` enum('language','library','framework','cms','tutorial','unit_test','git_hub','utilities','other') NOT NULL,
  `language` enum('html','css','javascript','typescript','php','sql','java','asp_dot_net') DEFAULT NULL,
  `library` enum('jquery','bootstrap') DEFAULT NULL,
  `framework_js` enum('angularjs','angular','vuejs','reactjs','react_native','emberjs','meteorjs') DEFAULT NULL,
  `framework_php` enum('symfony','laravel','sakephp','codeigniter','zend','yii') DEFAULT NULL,
  `cms` enum('wordpress','drupal','joomla','dotclear','weebly','squarespace','emonsite','ipaoo','sitew','cmonsite','jimbo','shopify','kiubi','orson') DEFAULT NULL,
  `love` int(255) DEFAULT NULL,
  `date_uploaded` date NOT NULL,
  `time_uploaded` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `link`
--

INSERT INTO `link` (`id_link`, `id_user`, `title`, `link`, `description`, `type`, `language`, `library`, `framework_js`, `framework_php`, `cms`, `love`, `date_uploaded`, `time_uploaded`) VALUES
(1, 12, 'Premier test', 'http://localhost/useful_link/src/profil.php', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus libero eros, laoreet quis condimentum et, vulputate nec magna. Phasellus vel imperdiet risus, et varius diam. Fusce eget urna ac nisl laoreet sollicitudin. Maecenas ac mi nulla. Aenean sed elementum sem. Pellentesque accumsan, dolor at mollis ultricies, lorem nunc sodales dui, nec fringilla ligula mauris at turpis. Quisque eget lectus ac ligula fringilla consequat et at mi. Donec ac purus magna.', 'other', 'typescript', NULL, 'angular', NULL, NULL, NULL, '2018-11-12', '00:00:00'),
(2, 12, 'Deuxième', 'http://localhost/useful_link/src/profil.php', 'zqerfg zaeiuy ghhqsiufvb zivbu sibuv isqdb visoudbv osiudb vsd Sdfv7 zv6s4 vvsdfmlvkjn qsoiuvjb sobnvu os bnvz', 'other', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11', '00:00:00'),
(3, 12, 'Test encore', 'http://localhost/useful_link/src/index.php', 'zegzefikezaygf izuygf izoeubf izoubf iouzbfn ouzdinbf zoudnbf ozufb zoufnb zouebnf zuebf dzuoibf zouidfb zoufb n', 'framework', 'javascript', 'bootstrap', 'angular', 'zend', 'squarespace', NULL, '2018-11-02', '20:52:34'),
(4, 12, 'sfgjsfgj', 'sfgjsfgj', 'sfgjsfgj', 'library', 'javascript', 'jquery', 'angular', 'laravel', 'drupal', NULL, '2018-11-02', '21:42:25'),
(5, 12, 'ergerg', 'ergerg', 'ergerge', 'library', 'javascript', 'bootstrap', 'vuejs', 'laravel', 'drupal', NULL, '2018-11-02', '21:43:16'),
(6, 12, 'rzgzergzer', 'zgsdg', 'sdgzgeg', 'other', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-02', '21:53:18'),
(7, 12, 'ezrgerg', 'ergerzgerger', 'ehetrrtyjretj', 'other', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-02', '21:53:47'),
(8, 12, 'ezrgerg', 'ergerzgerger', 'ehetrrtyjretj', 'other', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-02', '21:54:20'),
(9, 12, 'ezrgerg', 'ergerzgerger', 'ehetrrtyjretj', 'other', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-02', '21:58:10'),
(10, 12, '30 Seconds of CSS', 'https://30-seconds.github.io/30-seconds-of-css/', 'A curated collection of useful CSS snippets you can understand in 30 seconds or less.', 'language', 'css', NULL, NULL, NULL, NULL, NULL, '2018-11-02', '21:59:18'),
(11, 12, 'ZE%fgkze ofih ', 'rgpikzejn rgipozn gzopign z', 'pziefng ezopfgn ezopign eroping eir', 'other', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-02', '22:02:20');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(255) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('developper','former','student','ceo','cto','other') NOT NULL,
  `access` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `mail`, `password`, `status`, `access`) VALUES
(12, 'Aina', 'a.ramiarasoa@gmail.com', '$2y$10$/EnxWmiE7y0OYL8AsoPWD.olrR7DaSb/wyKHax5M.IMFdLp403kFC', 'developper', '2');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id_link`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `link`
--
ALTER TABLE `link`
  MODIFY `id_link` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `link`
--
ALTER TABLE `link`
  ADD CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

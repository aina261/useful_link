<?php

$title = "Your Profil";
require_once ('inc/init.php');



if ($_GET) {
    if (isset($_GET['aLink']) && $_GET['aLink'] == "delete" && is_numeric($_GET['id'])) {
        $result = $pdo->prepare('SELECT * FROM link WHERE id_link = :id_link');
        $result->bindValue(':id_link', $_GET['id'], PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() == 1) {
            $link = $result->fetch();
            $delete_req = "DELETE FROM link WHERE id_link = $link[id_link]";
            $delete_result = $pdo->exec($delete_req);
            if ($delete_result) {
                header('Location:?delete=success');
            } else {
                header('Location:?delete=failed');
            }

        }
    }

    if (isset($_GET['delete'])) {
        if ($_GET['delete'] == "success") {
            $msg .= "<div class='alert alert-success ml-5' role='alert'>The link has been successful deleted</div>";
        } else {
            $msg .= "<div class='alert alert-danger ml-5' role='alert'>An error has occured</div>";
        }
    }
}


if ($_SESSION) {
    if (isset($_SESSION['user'])) {
        foreach ($_SESSION['user'] as $key => $value) {
            if ($key == "user_name") {
                $profil .= "<p>User name : $value</p>";
            } elseif ($key == "mail") {
                $profil .= "<p>Mail : $value</p>";
            } elseif ($key == "status") {
                $profil .= "<p>Status : $value</p>";
            }
        }
    } else {
        header('Location:index.php');
        die();
    }
} else {
    header('Location:index.php');
    die();
}

$id = $_SESSION['user']['id_user'];

$result = $pdo->query("SELECT * FROM link WHERE id_user = $id ORDER BY id_link DESC");
$result->execute();
$links = $result->fetchAll();

foreach ($links as $link) {
    $linkCard .= "<div class='card mb-3'>";
    if ($link['og_img'] != NULL ) {
        $linkCard .= "<img class='card-img-top' src='$link[og_img]' alt='Card image cap'>";
    } else {
        $linkCard .= "<img class='card-img-top' src='" . URL . "asset/img/img_not_available.jpg' alt='Card image cap'>";
    }
    $linkCard .= "<div class='card-body profilSection'>";
    $linkCard .= "<h5 class='card-title'>$link[title]</h5>";
    $linkCard .= "<p class='card-text'>$link[description]</p>";
    $linkCard .= "<p>$link[link]</p>";
    $linkCard .= "</div>";
    $linkCard .= "<div class='card-footer'>";
    $linkCard .= "<a href='?edit=true&id=$link[id_link]'><i class='fas fa-edit fa-lg'></i></a>";
    $linkCard .= "<a id='deleteLinkUserProfil' data-toggle='modal' data-target='#deleteModal$link[id_link]'><i class='fas fa-ban fa-lg'></i></a>";
    $linkCard .= deleteModal($link['id_link'], $link['title'], "deleteLinkUser");
    $linkCard .= "</div>";
    $linkCard .= "</div>";
}


if($_POST) {
    if (isset($_POST['edit'])) {
        $req = "UPDATE user SET mail = :mail, status = :status WHERE id_user = :id_user";
        $result = $pdo->prepare($req);
        $result->bindValue(':mail', $_POST['mail'], PDO::PARAM_STR);
        $result->bindValue(':status', $_POST['status'], PDO::PARAM_STR);
        $result->bindValue(':id_user', $_POST['id_user'], PDO::PARAM_STR);

        if ($result->execute()) {
            unset($_SESSION['user']['mail']);
            unset($_SESSION['user']['status']);
            $_SESSION['user']['mail'] = $_POST['mail'];
            $_SESSION['user']['status'] = $_POST['status'];
            header('Location:?edit=success');
        } else {
            header('Location:?edit=failed');
        }
    }
}

if ($_GET) {
    if(isset($_GET['edit']) && $_GET['edit'] == 'success') {
        $msg = "<div class='alert alert-success ml-5' role='alert'>Your profil was successful updated</div>";
    }
    if (isset($_GET['up']) && $_GET['up'] == 'success') {
        $msg = "<div class='alert alert-success ml-5' role='alert'>Your link was successful uploaded</div>";
    }
    if (isset($_GET['modif']) && $_GET['modif'] == 'success') {
        $msg = "<div class='alert alert-success ml-5' role='alert'>Your link was successful updated</div>";
    }
}

require_once ('inc/addLinkForm.php');
require_once ('inc/header.php');


?>


    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row">
                    <h1><?= $title ?></h1>
                    <?= $msg ?>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 profilContent">
                        <div class="profilContent">
                            <?= $profil ?>
                        </div>
                        <div class="option">
                            <button type="button" id="editProfil" class="btn btn-secondary btn-sm">Edit Profil</button>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mt-4 mt-md-0 editProfilForm">
                        <form action="" method="post">
                            <input type="hidden" name="id_user" value="<?= $_SESSION['user']['id_user'] ?>">
                            <div class="form-group">
                                <label for="mailProfil">Email address</label>
                                <input type="email" class="form-control" id="mailProfil" name="mail" value="<?= $_SESSION['user']['mail'] ?>">
                                <div class="mailInfoContent">
                                    <span id="mailInfo"></span>
                                    <span id="mailInfo2"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status_edit">Who are you ?</label>
                                <select class="form-control" id="status_edit" name="status">
                                    <option value="developper" <?php if ($_SESSION['user']['status'] == "developper") { echo "selected"; } ?>>Developper</option>
                                    <option value="former" <?php if ($_SESSION['user']['status'] == "former") { echo "selected"; } ?>>Former</option>
                                    <option value="student" <?php if ($_SESSION['user']['status'] == "student") { echo "selected"; } ?>>Student</option>
                                    <option value="ceo" <?php if ($_SESSION['user']['status'] == "ceo") { echo "selected"; } ?>>CEO</option>
                                    <option value="cto" <?php if ($_SESSION['user']['status'] == "cto") { echo "selected"; } ?>>CTO</option>
                                    <option value="other" <?php if ($_SESSION['user']['status'] == "other") { echo "selected"; } ?>>Other</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-secondary btn-sm" name="edit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12 mb-5">
            <h3>Your uploaded link</h3>

        </div>
    </div>
<div class="row ">
    <div class="col-md-12 d-flex flex-wrap justify-content-center">
        <?= $linkCard ?>
    </div>
</div>




<?php require_once ('inc/footer.php'); ?>
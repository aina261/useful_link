<?php

$title = "Login";
require_once ('inc/init.php');

if ($_GET) {
    if (isset($_GET['reg']) && $_GET['reg'] == 'true') {
        $msg .= "<div class='alert alert-success' role='alert'>Thanks for your register, you can now login</div>";
    }
}

if ($_POST) {
    if (isset($_POST)) {
        $req = 'SELECT * FROM user WHERE user_name = :user_name';
        $result = $pdo->prepare($req);
        $result->bindValue(':user_name', $_POST['user_name'], PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() == 1) {
            $user = $result->fetch();

            if (password_verify($_POST['password'], $user['password'])) {
                foreach ($user as $key => $value) {
                    if ($key != 'password') {
                        $_SESSION['user'][$key] = $value;
                        header('Location:index.php');
                    }
                }
            } else {
                $error .= "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
                $error .= "An error occured, please retry.";
                $error .= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
                $error .= "<span aria-hidden='true'>&times;</span>";
                $error .= "</button>";
                $error .= "</div>";
            }
        } else {
            $error .= "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
            $error .= "An error occured, please retry.";
            $error .= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            $error .= "<span aria-hidden='true'>&times;</span>";
            $error .= "</button>";
            $error .= "</div>";
        }

    }
}





require_once ('inc/header.php');


?>

    <div class="row">
        <div class="col-12 offset-md-3 col-md-6">
            <?= $msg ?>
            <?= $error ?>
            <h1><?= $title ?></h1>

            <div class="alert alert-danger error" role="alert">
                Please fill all fields
            </div>

            <form id="formLogin" action="" method="post">

                <div class="form-group">
                    <label for="user_name_login">User name</label>
                    <input type="text" class="form-control" id="user_name_login" name="user_name" placeholder="Enter your user name here">
                    <span id="userNameInfo"></span>
                </div>

                <div class="form-group">
                    <label for="password_login">Password <span>Password</span></label>
                    <input type="password" class="form-control" id="password_login" name="password" placeholder="Enter your amazing really hard password here">
                    <span id="passwordInfo"></span>
                </div>

                <button id="submitLogin" type="submit" class="btn btn-outline-warning">Login</button>

            </form>
        </div>
    </div>




<?php require_once ('inc/footer.php'); ?>
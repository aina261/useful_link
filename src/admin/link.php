<?php

require_once ('../inc/init.php');

if (!isset($_SESSION['user']) && (!$_SESSION['user']['access'] == '1' || !$_SESSION['user']['access'] == '2')) {
    header('Location:' . URL);
    die();
}


if ($_GET) {
    if (isset($_GET['aLink']) && $_GET['aLink'] == "delete" && is_numeric($_GET['id'])) {
        $result = $pdo->prepare('SELECT * FROM link WHERE id_link = :id_link');
        $result->bindValue(':id_link', $_GET['id'], PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() == 1) {
            $link = $result->fetch();
            $delete_req = "DELETE FROM link WHERE id_link = $link[id_link]";
            $delete_result = $pdo->exec($delete_req);
            if ($delete_result) {
                header('Location:?delete=success');
            } else {
                header('Location:?delete=failed');
            }

        }
    }

    if (isset($_GET['delete'])) {
        if ($_GET['delete'] == "success") {
            $msg .= "<div class='alert alert-success ml-5' role='alert'>The link has been successful deleted</div>";
        } else {
            $msg .= "<div class='alert alert-danger ml-5' role='alert'>An error has occured</div>";
        }
    }
}


$result = $pdo->query("SELECT * FROM link ORDER BY id_link DESC");
$result->execute();
$links = $result->fetchAll();
foreach ($links as $link) {
    $linkCard .= "<div class='card mb-3'>";
    if ($link['og_img'] != NULL ) {
        $linkCard .= "<img class='card-img-top' src='$link[og_img]' alt='Card image cap'>";
    } else {
        $linkCard .= "<img class='card-img-top' src='" . URL . "asset/img/img_not_available.jpg' alt='Card image cap'>";
    }
    $linkCard .= "<div class='card-body profilSection'>";
    $linkCard .= "<h5 class='card-title'>$link[title]</h5>";
    $linkCard .= "<p class='card-text'>$link[description]</p>";
    $linkCard .= "<p>$link[link]</p>";
    $linkCard .= "</div>";
    $linkCard .= "<div class='card-footer'>";
    $linkCard .= "<a href='?edit=true&id=$link[id_link]'><i class='fas fa-edit fa-lg'></i></a>";
    $linkCard .= "<a id='deleteLinkUserProfil' data-toggle='modal' data-target='#deleteModal$link[id_link]'><i class='fas fa-ban fa-lg'></i></a>";
    $linkCard .= deleteModal($link['id_link'], $link['title'], "deleteLinkUser");
    $linkCard .= "</div>";
    $linkCard .= "</div>";
}






require_once ('inc/header.php');
?>

<div class="col-12">
    <?= $msg ?>
    <?= $linkCard ?>
</div>


<?php
require_once ('inc/footer.php');
?>
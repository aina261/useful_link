<?php


    require_once ('init.php');
    $pdo = bddConnect();

    function validateUser($pdo, $get) {

        $result = $pdo->prepare('SELECT * FROM user WHERE user_name = :user_name');
        $result->bindValue(':user_name', $get['user_name'], PDO::PARAM_STR);
        $result->execute();

        if ($result->fetch()) {
            return array (
              'success' => true
            );
        } else {
            return array (
                'success' => false
            );
        }
    }

    header('Content-Type: application/json');
    echo json_encode(validateUser($pdo, $_GET), JSON_PRETTY_PRINT);
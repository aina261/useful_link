<?php

$title = "Register";

require_once ('inc/init.php');


if ($_POST) {
    if (isset($_POST)) {

        debug($_POST);

        $req = "SELECT * FROM user WHERE user_name = :user_name";
        $result = $pdo->prepare($req);
        $result->bindValue(':user_name', $_POST['user_name'], PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() == 1 ) {
            header('Location:?reg=false&type=user_name');
        }

        $req = "SELECT * FROM user WHERE mail= :mail";
        $result = $pdo->prepare($req);
        $result->bindValue(':mail', $_POST['mail'], PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() == 1 ) {
            header('Location:?reg=false&type=mail');
        }

        $req = "INSERT INTO user (user_name, mail, password, status, access) VALUES (:user_name, :mail, :password, :status, '0')";
        $result = $pdo->prepare($req);

        $password_hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $result->bindValue(':user_name', $_POST['user_name'], PDO::PARAM_STR);
        $result->bindValue(':mail', $_POST['mail'], PDO::PARAM_STR);
        $result->bindValue(':password', $password_hash, PDO::PARAM_STR);
        $result->bindValue(':status', $_POST['status'], PDO::PARAM_STR);

        if ($result->execute()) {
            header('Location:login.php?reg=true');
        } else {
            $error .= "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
            $error .= "An error occured, please retry.";
            $error .= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            $error .= "<span aria-hidden='true'>&times;</span>";
            $error .= "</button>";
            $error .= "</div>";
        }
    }
}

require_once ('inc/header.php');
?>

    <!--<div id="menuOverlay">

    </div>-->

    <div class="row">
        <div class="col-12 offset-md-3 col-md-6">
            <h1>Cool, a future new slave for my link bank ... </h1>
            <div class="alert alert-danger error" role="alert">
                Please fill all fields
            </div>
            <?= $error ?>
            <form id="formRegister" action="" method="post">

                <div class="form-group">
                    <label for="user_name_register">User name</label>
                    <input type="text" class="form-control" id="user_name_register" name="user_name" placeholder="Enter your user name here">
                    <span id="userNameInfo"></span>
                </div>

                <div class="form-group">
                    <label for="mail_register">Mail <span>Just for send messages every day</span></label>
                    <input type="email" class="form-control" id="mail_register" name="mail" placeholder="Enter your mail here">
                    <div class="mailInfoContent">
                        <span id="mailInfo"></span>
                        <span id="mailInfo2"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password_register">Password <span>Leave your password you use everywhere, promised, I wouldn't look ...</span></label>
                    <input type="password" class="form-control" id="password_register" name="password" placeholder="Enter your amazing really hard password here">
                    <span id="passwordInfo"></span>

                    <div class="infoPwd">
                        <div class="arrowUp"></div>
                        <div class="infoContent">
                            <p>Your password must be have least 5 characters long.</p>
                            <p>For a cool password, you need to have : <span>(not necessary)</span></p>
                            <ul>
                                <li>Capital letters</li>
                                <li>Lower letters</li>
                                <li>Numbers</li>
                                <li>Special characters</li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="status_register">Who are you ? <span>If you are CEO, I will contact you very soon ... I NEED MONEY</span></label>
                    <select class="form-control" id="status_register" name="status">
                        <option value="developper">Developper</option>
                        <option value="former">Former</option>
                        <option value="student">Student</option>
                        <option value="ceo">CEO</option>
                        <option value="cto">CTO</option>
                        <option value="other">Other</option>
                    </select>
                </div>

                <button id="submitRegister" type="submit" class="btn btn-outline-warning">Submit</button>

            </form>
        </div>
    </div>




<?php require_once ('inc/footer.php'); ?>
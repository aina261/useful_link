<?php


require_once ('init.php');
$pdo = bddConnect();

function validateUser($pdo, $get) {

    $result = $pdo->prepare('SELECT * FROM user WHERE mail = :mail');
    $result->bindValue(':mail', $get['mail'], PDO::PARAM_STR);
    $result->execute();

    if ($result->fetch()) {
        return array (
            'success' => true
        );
    } else {
        return array (
            'success' => false
        );
    }

}

header('Content-Type: application/json');
echo json_encode(validateUser($pdo, $_GET), JSON_PRETTY_PRINT);
<?php

if ($_POST) {

    if (isset($_POST)) {

        if (isset($_POST['addLinkForm'])) {
            $req = ('INSERT INTO link (id_user, title, link, description, type, language, library, framework_js, framework_php, cms, date_uploaded, time_uploaded, og_img) 
                VALUES (:id_user, :title, :link, :description, :type, :language, :library, :framework_js, :framework_php, :cms, CURDATE(), CURTIME(), :og_img)');
            $result = $pdo->prepare($req);
        }

        if (isset($_POST['modifLinkForm'])) {

            $req = ('UPDATE link 
                SET id_user = :id_user, 
                    title = :title, 
                    link = :link, 
                    description = :description, 
                    type = :type, 
                    language = :language, 
                    library = :library, 
                    framework_js = :framework_js, 
                    framework_php = :framework_php, 
                    cms = :cms, 
                    date_uploaded = CURDATE(), 
                    time_uploaded = CURTIME(),
                    og_img = :og_img
                WHERE id_link = :id_link ');
            $result = $pdo->prepare($req);
            $result->bindValue(':id_link', $_POST['id_link'], PDO::PARAM_INT);
        }


        $result->bindValue(':id_user', $_SESSION['user']['id_user'], PDO::PARAM_INT);
        $result->bindValue(':title', $_POST['title'], PDO::PARAM_STR);
        $result->bindValue(':link', $_POST['link'], PDO::PARAM_STR);
        $result->bindValue(':description', $_POST['description'], PDO::PARAM_STR);
        $result->bindValue(':type', $_POST['type'], PDO::PARAM_STR);

        if (!empty($_POST['language'])) {
            $result->bindValue(':language', $_POST['language'], PDO::PARAM_STR);
        } else {
            $result->bindValue(':language', NULL, PDO::PARAM_NULL);
        }
        if (!empty($_POST['library'])) {
            $result->bindValue(':library', $_POST['library'], PDO::PARAM_STR);
        } else {
            $result->bindValue(':library', NULL, PDO::PARAM_NULL);
        }
        if (!empty($_POST['framework_js'])) {
            $result->bindValue(':framework_js', $_POST['framework_js'], PDO::PARAM_STR);
        } else {
            $result->bindValue(':framework_js', NULL, PDO::PARAM_NULL);
        }
        if (!empty($_POST['framework_php'])) {
            $result->bindValue(':framework_php', $_POST['framework_php'], PDO::PARAM_STR);
        } else {
            $result->bindValue(':framework_php', NULL, PDO::PARAM_NULL);
        }
        if (!empty($_POST['cms'])) {
            $result->bindValue(':cms', $_POST['cms'], PDO::PARAM_STR);
        } else {
            $result->bindValue(':cms', NULL, PDO::PARAM_NULL);
        }

        $ogImg = get_og_image($_POST['link']); // Get og:image link

        if (linkG($ogImg)) { // If og:image start with http or https
            $result->bindValue(':og_img', get_og_image($_POST['link']), PDO::PARAM_STR);
        } else {
            $result->bindValue(':og_img', NULL, PDO::PARAM_NULL);
        }

        debug($_POST);

        if ( isset($_POST['addLinkForm']) && $result->execute()) {
            header('Location:?up=success');
        }
        if ( isset($_POST['modifLinkForm']) && $result->execute()) {
            header('Location:?modif=success');
        } else {
            header('Location?modif=failed');
        }
    }
}

$modif = "addLinkForm";
$modifVal = "Upload";
$addOrEdit = "Add new link";
if ($_GET) {
    if (isset($_GET['edit']) && $_GET['edit'] == "true") {
        $modif = "modifLinkForm";
        $modifVal = "Edit";
        $addOrEdit = "Edit link";
        $req = "SELECT * FROM link WHERE id_link = :id_link";
        $result = $pdo->prepare($req);
        $result->bindValue(':id_link', $_GET['id'], PDO::PARAM_INT);
        $result->execute();
        if ($result->rowCount() === 1) {
            $linkData = $result->fetch();
        } else {
            header('Location:profil.php?id=false');
        }
    }
}

$newLinkForm .= "<div class='row addMoreLink'>";
$newLinkForm .= "<div class='col-12'>";
$newLinkForm .= "<div class='infoButton btn btn-secondary'>";
$newLinkForm .= "$addOrEdit";
$newLinkForm .= "</div>";
$newLinkForm .= "<button type='button' id='buttonAddLink' class='btn btn-secondary'><i class='fas fa-plus'></i></button>";
$newLinkForm .= "</div>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='row uploaded'>";
$newLinkForm .= "<div class='col-md-12'>";
$newLinkForm .= "<form id='uploadedForm' action='' method='post'>";
$newLinkForm .= "<div class='row'>";
$newLinkForm .= "<div class='col-12 col-md-6'>";
if ($_GET) {
    if (isset($_GET['edit']) && $_GET['edit'] == 'true') {
        $newLinkForm .= "<input type='hidden' class='form-control' id='id_link' name='id_link' value='$_GET[id]'>";
    }
}
$newLinkForm .= "<label for='title'>Title</label>";
if (!empty($linkData['title'])) { $newLinkForm .= "<input type='text' class='form-control' id='title' name='title' value='$linkData[title]' placeholder='Title'>";} else { $newLinkForm .= "<input type='text' class='form-control' id='title' name='title' value='' placeholder='Title'>";}
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='col-12 col-md-6'>";
$newLinkForm .= "<label for='link'>Link</label>";
if (!empty($linkData['link'])) { $newLinkForm .= "<input type='text' class='form-control' id='link' name='link' value='$linkData[link]' placeholder='Link'>";} else { $newLinkForm .= "<input type='text' class='form-control' id='link' name='link' placeholder='Link'>";}
$newLinkForm .= "</div>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='form-group mb-0'>";
$newLinkForm .= "<label for='description'>Description <span id='descCount'></span></label>";
if (!empty($linkData['description'])) { $newLinkForm .= "<input type='text' class='form-control' maxlength='255' id='description' name='description' value='$linkData[description]' placeholder='Description'>";} else { $newLinkForm .= "<input type='text' class='form-control' maxlength='255' id='description' name='description' placeholder='Description'>";}
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='row'>";
$newLinkForm .= "<div class='col-12 col-md-4'>";
$newLinkForm .= "<label for='type'>Type</label>";
$newLinkForm .= "<select class='form-control' id='type' name='type'>";
$newLinkForm .= "<option value='-- Make your choice --'>-- Make your choice --</option>";
if (!empty($linkData['type']) && $linkData['type'] == "language") { $newLinkForm .= "<option value='language' selected>Language</option>";} else { $newLinkForm .= "<option value='language'>Language</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "library") { $newLinkForm .= "<option value='library' selected>Library</option>";} else { $newLinkForm .= "<option value='library'>Library</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "framework") { $newLinkForm .= "<option value='framework' selected>Framework</option>";} else { $newLinkForm .= "<option value='framework'>Framework</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "cms") { $newLinkForm .= "<option value='cms' selected>CMS</option>";} else { $newLinkForm .= "<option value='cms'>CMS</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "unit_test") { $newLinkForm .= "<option value='unit_test' selected>Unit Test</option>";} else { $newLinkForm .= "<option value='unit_test'>Unit Test</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "tutorial") { $newLinkForm .= "<option value='tutorial' selected>Tutorial</option>";} else { $newLinkForm .= "<option value='tutorial'>Tutorial</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "git_hub") { $newLinkForm .= "<option value='git_hub' selected>GitHub</option>";} else { $newLinkForm .= "<option value='git_hub'>GitHub</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "utilities") { $newLinkForm .= "<option value='utilities' selected>Utilities</option>";} else { $newLinkForm .= "<option value='utilities'>Utilities</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "back") { $newLinkForm .= "<option value='back' selected>Back</option>";} else { $newLinkForm .= "<option value='back'>Back</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "front") { $newLinkForm .= "<option value='front' selected>Front</option>";} else { $newLinkForm .= "<option value='front'>Front</option>"; }
if (!empty($linkData['type']) && $linkData['type'] == "other") { $newLinkForm .= "<option value='other' selected>Other</option>";} else { $newLinkForm .= "<option value='other'>Other</option>"; }
$newLinkForm .= "</select>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='col-12 col-md-4'>";
$newLinkForm .= "<label for='language'>Language</label>";
$newLinkForm .= "<select class='form-control' id='language' name='language'>";
$newLinkForm .= "<option value=''>-- Make your choice --</option>";
if (!empty($linkData['type']) && $linkData['language'] == "html") { $newLinkForm .= "<option value='html' selected>HTML</option>";} else { $newLinkForm .= "<option value='html'>HTML</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "css") { $newLinkForm .= "<option value='css' selected>CSS</option>";} else { $newLinkForm .= "<option value='css'>CSS</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "javascript") { $newLinkForm .= "<option value='javascript' selected>Java Script</option>";} else { $newLinkForm .= "<option value='javascript'>Java Script</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "typescript") { $newLinkForm .= "<option value='typescript' selected>Type Script</option>";} else { $newLinkForm .= "<option value='typescript'>Type Script</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "php") { $newLinkForm .= "<option value='php' selected>PHP</option>";} else { $newLinkForm .= "<option value='php'>PHP</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "sql") { $newLinkForm .= "<option value='sql' selected>SQL</option>";} else { $newLinkForm .= "<option value='sql'>SQL</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "java") { $newLinkForm .= "<option value='java' selected>Java</option>";} else { $newLinkForm .= "<option value='java'>Java</option>"; }
if (!empty($linkData['type']) && $linkData['language'] == "asp_dot_net") { $newLinkForm .= "<option value='asp_dot_net' selected>ASP.net</option>";} else { $newLinkForm .= "<option value='asp_dot_net'>ASP.net</option>"; }
$newLinkForm .= "</select>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='col-12 col-md-4'>";
$newLinkForm .= "<label for='library'>Library</label>";
$newLinkForm .= "<select class='form-control' id='library' name='library'>";
$newLinkForm .= "<option value=''>-- Make your choice --</option>";
if (!empty($linkData['type']) && $linkData['library'] == "jquery") { $newLinkForm .= "<option value='jquery' selected>jQuery</option>";} else { $newLinkForm .= "<option value='jquery'>jQuery</option>"; }
if (!empty($linkData['type']) && $linkData['library'] == "bootstrap") { $newLinkForm .= "<option value='bootstrap' selected>Bootstrap</option>";} else { $newLinkForm .= "<option value='bootstrap'>Bootstrap</option>"; }
$newLinkForm .= "</select>";
$newLinkForm .= "</div>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='row'>";
$newLinkForm .= "<div class='col-12 col-md-4'>";
$newLinkForm .= "<label for='framework_js'>Framework js</label>";
$newLinkForm .= "<select class='form-control' id='framework_js' name='framework_js'>";
$newLinkForm .= "<option value=''>-- Make your choice --</option>";
if (!empty($linkData['type']) && $linkData['framework_js'] == "angularjs") { $newLinkForm .= "<option value='angularjs' selected>AngularJS</option>";} else { $newLinkForm .= "<option value='angularjs'>AngularJS</option>"; }
if (!empty($linkData['type']) && $linkData['framework_js'] == "angular") { $newLinkForm .= "<option value='angular' selected>Angular</option>";} else { $newLinkForm .= "<option value='angular'>Angular</option>"; }
if (!empty($linkData['type']) && $linkData['framework_js'] == "vuejs") { $newLinkForm .= "<option value='vuejs' selected>VueJS</option>";} else { $newLinkForm .= "<option value='vuejs'>VueJS</option>"; }
if (!empty($linkData['type']) && $linkData['framework_js'] == "reactjs") { $newLinkForm .= "<option value='reactjs' selected>ReactJS</option>";} else { $newLinkForm .= "<option value='reactjs'>ReactJS</option>"; }
if (!empty($linkData['type']) && $linkData['framework_js'] == "react_native") { $newLinkForm .= "<option value='react_native' selected>React Native</option>";} else { $newLinkForm .= "<option value='react_native'>React Native</option>"; }
if (!empty($linkData['type']) && $linkData['framework_js'] == "emberjs") { $newLinkForm .= "<option value='emberjs' selected>EmberJS</option>";} else { $newLinkForm .= "<option value='emberjs'>EmberJS</option>"; }
if (!empty($linkData['type']) && $linkData['framework_js'] == "meteorjs") { $newLinkForm .= "<option value='meteorjs' selected>MeteoJS</option>";} else { $newLinkForm .= "<option value='meteorjs'>MeteoJS</option>"; }
$newLinkForm .= "</select>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='col-12 col-md-4'>";
$newLinkForm .= "<label for='framework_php'>Framwork php</label>";
$newLinkForm .= "<select class='form-control' id='framework_php' name='framework_php'>";
$newLinkForm .= "<option value=''>-- Make your choice --</option>";
if (!empty($linkData['type']) && $linkData['framework_php'] == "symfony") { $newLinkForm .= "<option value='symfony' selected>Symfony</option>";} else { $newLinkForm .= "<option value='symfony'>Symfony</option>"; }
if (!empty($linkData['type']) && $linkData['framework_php'] == "laravel") { $newLinkForm .= "<option value='laravel' selected>Laravel</option>";} else { $newLinkForm .= "<option value='laravel'>Laravel</option>"; }
if (!empty($linkData['type']) && $linkData['framework_php'] == "cakephp") { $newLinkForm .= "<option value='cakephp' selected>CakePHP</option>";} else { $newLinkForm .= "<option value='cakephp'>CakePHP</option>"; }
if (!empty($linkData['type']) && $linkData['framework_php'] == "codeigniter") { $newLinkForm .= "<option value='codeigniter' selected>CodeIgniter</option>";} else { $newLinkForm .= "<option value='codeigniter'>CodeIgniter</option>"; }
if (!empty($linkData['type']) && $linkData['framework_php'] == "zend") { $newLinkForm .= "<option value='zend' selected>Zend</option>";} else { $newLinkForm .= "<option value='zend'>Zend</option>"; }
if (!empty($linkData['type']) && $linkData['framework_php'] == "yii") { $newLinkForm .= "<option value='yii' selected>YII</option>";} else { $newLinkForm .= "<option value='yii'>YII</option>"; }
$newLinkForm .= "</select>";
$newLinkForm .= "</div>";
$newLinkForm .= "<div class='col-12 col-md-4'>";
$newLinkForm .= "<label for='cms'>CMS</label>";
$newLinkForm .= "<select class='form-control' id='cms' name='cms'>";
$newLinkForm .= "<option value=''>-- Make your choice --</option>";
if (!empty($linkData['type']) && $linkData['cms'] == "wordpress") { $newLinkForm .= "<option value='wordpress' selected>Wordpress</option>";} else { $newLinkForm .= "<option value='wordpress'>Wordpress</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "drupal") { $newLinkForm .= "<option value='drupal' selected>Drupal</option>";} else { $newLinkForm .= "<option value='drupal'>Drupal</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "joomla") { $newLinkForm .= "<option value='joomla' selected>Joomla</option>";} else { $newLinkForm .= "<option value='joomla'>Joomla</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "botclear") { $newLinkForm .= "<option value='botclear' selected>DotClear</option>";} else { $newLinkForm .= "<option value='botclear'>DotClear</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "weebly") { $newLinkForm .= "<option value='weebly' selected>Weebly</option>";} else { $newLinkForm .= "<option value='weebly'>Weebly</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "squarespace") { $newLinkForm .= "<option value='squarespace' selected>Square Space</option>";} else { $newLinkForm .= "<option value='squarespace'>Square Space</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "emonsite") { $newLinkForm .= "<option value='emonsite' selected>E-monsite</option>";} else { $newLinkForm .= "<option value='emonsite'>E-monsite</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "ipaoo") { $newLinkForm .= "<option value='ipaoo' selected>IPAOO</option>";} else { $newLinkForm .= "<option value='ipaoo'>IPAOO</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "sitew") { $newLinkForm .= "<option value='sitew' selected>Site W</option>";} else { $newLinkForm .= "<option value='sitew'>Site W</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "cmonsite") { $newLinkForm .= "<option value='cmonsite' selected>CmonSite</option>";} else { $newLinkForm .= "<option value='cmonsite'>CmonSite</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "jimbo") { $newLinkForm .= "<option value='jimbo' selected>Jimbo</option>";} else { $newLinkForm .= "<option value='jimbo'>Jimbo</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "shopify") { $newLinkForm .= "<option value='shopify' selected>Shopify</option>";} else { $newLinkForm .= "<option value='shopify'>Shopify</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "kiubi") { $newLinkForm .= "<option value='kiubi' selected>Kiubi</option>";} else { $newLinkForm .= "<option value='kiubi'>Kiubi</option>"; }
if (!empty($linkData['type']) && $linkData['cms'] == "orson") { $newLinkForm .= "<option value='orson' selected>Orson</option>";} else { $newLinkForm .= "<option value='orson'>Orson</option>"; }
$newLinkForm .= "</select>";
$newLinkForm .= "</div>";
$newLinkForm .= "</div>";
$newLinkForm .= "<button type='submit' name='$modif' class='btn btn-secondary w-100 mt-3'>$modifVal</button>";
$newLinkForm .= "</form>";
$newLinkForm .= "</div>";
$newLinkForm .= "</div>";
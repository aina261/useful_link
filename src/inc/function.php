<?php

function bddConnect () {
    $dsn = "mysql:host=localhost:3306; dbname=useful_link";
    $log = "root";
    $pwd = "";
    $attributes = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    return $pdo = new PDO($dsn, $log, $pwd, $attributes);
}

function debug($var, $mode = 1) {
    echo "<div class='alert alert-warning'>";
    $trace = debug_backtrace();
    $trace = array_shift($trace);
    echo "$trace[file]<br> line $trace[line] <hr>";
    echo "<pre>";
    switch ($mode) {
        case '1':
            var_dump($var);
            break;
        default:
            print_r($var);
            break;
    }
    echo "</pre>";
    echo "</div>";
}



function linkG($link) {
    if (preg_match("#(http://?|https://)#", $link)) {
        return true;
    } else {
        return false;
    }
}




$deleteGet = "";
function deleteModal($id, $title, $infoDelete) {
    $modalDelete = '';
    $modalDelete .= '<div class="modal fade" id="deleteModal' . $id . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
    $modalDelete .= '<div class="modal-dialog" role="document">';
    $modalDelete .= '<div class="modal-content">';
    $modalDelete .= '<div class="modal-header">';
    $modalDelete .= "<h5 class='modal-title' id='exampleModalLabel'>Delete</h5>";
    $modalDelete .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    $modalDelete .= '<span aria-hidden="true">&times;</span>';
    $modalDelete .= '</button>';
    $modalDelete .= '</div>';
    $modalDelete .= '<div class="modal-body">';
    switch ($infoDelete) {
        case "deleteLinkUser":
            $modalDelete .= "Do you really want to delete this link ?";
            $modalDelete .= $title;
            $deleteGet = "?aLink=delete&id=$id";
            break;
        case "deleteLinkAdmin";
            $modalDelete .= "Do you really want to delete this link ?";
            $modalDelete .= $title;
            $deleteGet = "?aLink=delete&id=$id";
            break;
        case "deleteUserAdmin":
            $modalDelete .= "Do you really want to delete this user ?";
            $modalDelete .= $title;
            $deleteGet = "?aUser=delete&id=$id";
            break;
        default:
            $deleteGet = "";
            break;
    }
    $modalDelete .= '</div>';
    $modalDelete .= '<div class="modal-footer">';
    $modalDelete .= '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>';
    $modalDelete .= '<a href="' . $deleteGet . '" class="btn btn-danger">Delete</a>';
    $modalDelete .= '</div>';
    $modalDelete .= '</div>';
    $modalDelete .= '</div>';
    $modalDelete .= '</div>';

    return $modalDelete;
}




function localizedLink($where, $info) {
    $content = "";
    $pdo = bddConnect();
    $req = "SELECT * FROM link WHERE $where = '$info' ORDER BY id_link DESC";
    $result = $pdo->query($req);
    $result->execute();

    if ($result->rowCount() >= 1) {
        $links = $result->fetchAll();
        foreach ($links as $link) {

            $id_link = $link['id_link'];

            $user = $pdo->query("SELECT user_name 
                                  FROM user 
                                  WHERE id_user = (
                                      SELECT id_user
                                      FROM link 
                                      WHERE id_link = $id_link)");
            $user->execute();
            $user = $user->fetch();

            $linkG = "";
            if ($link['link']) {
                if (linkG($link['link'])) {
                    $linkG = $link['link'];
                } else {
                    $linkG = "http://" . $link['link'];
                }
            }

            $content .= "<div class='card mb-3'>";
            if ($link['og_img'] != NULL ) {
                $content .= "<img class='card-img-top' src='$link[og_img]' alt='Card image cap'>";
            } else {
                $content .= "<img class='card-img-top' src='" . URL . "asset/img/img_not_available.jpg' alt='Card image cap'>";
            }
            $content .= "<p class='sending'>Sending by $user[user_name] @ $link[date_uploaded] - $link[time_uploaded]</p>";
            $content .= "<div class='card-body'>";
            $content .= "<h5 class='card-title'>$link[title]</h5>";
            $content .= "<p class='card-text'>$link[description]</p>";
            $content .= "<a href='$linkG' target='_blank' class='btn btn-secondary'>Follow the link</a>";
            $content .= "</div>";
            $content .= "<div class='card-footer'>";
            if (isset($link['type'])) {
                $content .= "<a href='localized.php?type=$link[type]'>$link[type]</a>";
            }
            if (isset($link['language'])) {
                $content .= "<a href='localized.php?language=$link[language]'>$link[language]</a>";
            }
            if (isset($link['library'])) {
                $content .= "<a href='localized.php?library=$link[library]'>$link[library]</a>";
            }
            if (isset($link['framework_js'])) {
                $content .= "<a href='localized.php?frameworkjs=$link[framework_js]'>$link[framework_js]</a>";
            }
            if (isset($link['framwork_php'])) {
                $content .= "<a href='localized.php?frameworkphp=$link[framwork_php]'>$link[framwork_php]</a>";
            }
            if (isset($link['cms'])) {
                $content .= "<a href='localized.php?cms=$link[cms]'>$link[cms]</a>";
            }
            $content .= "</div>";
            $content .= "</div>";
        }
    } else {
        $content .= "<div><p>Sorry, there is no link for the moment, don't hesitate to add it</p></div>";
    }

    return $content;
}




// Get og:image => link
function get_og_image($url) {
    $site_html=  file_get_contents($url);
    $matches=null;
    preg_match_all('~<\s*meta\s+property="(og:[^"]+)"\s+content="([^"]*)~i', $site_html,$matches);
    $ogtags=array();
    for($i=0;$i<count($matches[1]);$i++) {
        $ogtags[$matches[1][$i]]=$matches[2][$i];
    }
    if (isset($ogtags["og:image"])) {
        return $ogtags["og:image"];
    } else {
        return NULL;
    }
}
<?php
require_once ('../inc/init.php');

$result = $pdo->query('SELECT * FROM link');
$result->execute();
$links = $result->rowCount();

$result = $pdo->query('SELECT * FROM user');
$result->execute();
$users = $result->rowCount();


$bddInfo .= "<ul class='list-group list-group-flush mt-5'>";
$bddInfo .= "<li class='list-group-item'>There is currently $links links uploaded</li>";
$bddInfo .= "<li class='list-group-item mt-4'>There is currently $users users registered</li>";
$bddInfo .= "</ul>";

require_once ('inc/header.php');

?>


        <div class="col-6">
            <?= $bddInfo ?>
        </div>

        <div class="googleDashbord">
            <div id="embed-api-auth-container"></div>
            <div id="chart-container"></div>
            <div id="view-selector-container"></div>
        </div>


<?php
require_once ('inc/footer.php');
?>
